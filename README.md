<div align="center">
<img src="/static/icon.png" align="center" width="150" height="150" alt="Project icon">

# VikingUI

A pack of addons for ESO that not only changes your UI, but the way you'll be experiencing the game. Inspired by SpartanUI for World of Warcraft.

[![pipeline status](https://gitlab.com/Seranth/vikingui/badges/master/pipeline.svg)](https://gitlab.com/Seranth/vikingui/commits/master)
[![coverage report](https://gitlab.com/Seranth/vikingui/badges/master/coverage.svg)](https://gitlab.com/Seranth/vikingui/commits/master)
</div>

